//! This is module documentation. My library is so nice!

/// four() is a function that returns `4`
/// ```
/// use mylib::four;
/// let x = four();
/// assert_eq!(x, 4);
/// ```
pub fn four() -> i32 { 4 }

#[cfg(test)]
mod tests {
    use super::four;
    #[test]
    fn it_works() {
        assert_eq!(four(), 4);
    }
}
